import React, { Component } from 'react';
import { Router, Route, Switch } from "react-router";
import Services from './Services';
// import { BrowserRouter as Router, Route, Link } from "react-router-dom";


class Home extends Component {
    render() {
        return (
            <div> 
                <h1 style={{color:'blue'}}> Home page</h1>
                <Services />
            </div>
        );
    }
}

export default Home;