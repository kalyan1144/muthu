import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Home from './view/Home'; 
import About from './view/About';
import Contact from './view/Contact';
import {
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div className="App">
        
            <div >
            <Link to="/Contact">Contact us</Link><br></br>
            <Link to="/Home">Home</Link><br/>
              <Link to="/About">About us 1</Link><br/>   
              <br/><br/><br/>
              <Switch>
                    <Route exact path="/Home" component={Home} />
                    <Route path="/About" component={About} />
                    <Route path="/" component= {Contact} />
                    <Route path="/Contact" component= {Contact} />
                    <Redirect to="/" />
              </Switch>
            </div>
      </div>
    );
  }
}

export default App;
